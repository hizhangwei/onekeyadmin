<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\Db;
use think\facade\View;
use app\addons\File;
use app\admin\BaseController;
/**
 * 一键生成代码
 */
class Curd extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if ($this->request->isPost()) {
            return json(['status' => 'success', 'data' => $this->request->publicMenu]);
        } else {
            $tables = Db::query("SHOW TABLES LIKE '%mk_app_%'");
            View::assign('tables', array_column($tables,'Tables_in_'.env('database.database').' (%mk_app_%)'));
            return View::fetch();
        }
    }

    /**
     * 生成代码
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $input = input('post.');
            $pluginPath = plugin_path() . $input['name'] . '/';
            // 生成插件文件夹
            File::dirMkdir($pluginPath);
            // 生成插件基础信息
            $pluginInfo = [
                'title'  => $input['title'],
                'name'   => $input['name'],
                'status' => 1,
                'sort'   => 0,
            ];
            File::create($pluginPath.'info.php', "<?php\nreturn ".var_export($pluginInfo,true).";");
            // 生成插件菜单图片
            if (! empty($input['cover'])) {
                File::create($pluginPath.'menu.png', file_get_contents(public_path() . $input['cover']));
            }
            // 生成后台控制器、模型、视图、菜单权限、API接口、前端路由
            $menuChildren = [];
            $pluginRoute  = [];
            foreach ($input['table'] as $key => $value) {
                $table = str_replace('mk_','', $value);
                $viewPath = str_replace('mk_app_','', $value);
                $path  = ucwords(str_replace('_', ' ', $viewPath));
                $path  = str_replace(' ','',lcfirst($path));
                $class = ucfirst($path);
                $field = Db::query("SHOW FULL COLUMNS FROM ".$value."");
                include(app_path() . "addons/Curd.php");
                // 后台
                if (! is_file($pluginPath . "admin/view/$viewPath/index.html")) File::create($pluginPath . "admin/view/$viewPath/index.html", $adminView);
                if (! is_file($pluginPath . "admin/controller/$class.php")) File::create($pluginPath . "admin/controller/$class.php", $adminController);
                if (! is_file($pluginPath . "admin/model/$class.php")) File::create($pluginPath . "admin/model/$class.php", $admminModel);
                // 前台
                $indexView = "{include file='common/header'}\n请编写您的视图，头尾文件是引入当前默认主题的，你也可以不使用\n{include file='common/footer'}";
                if (! is_file($pluginPath . "index/view/catalog/".$viewPath.".html")) File::create($pluginPath . "index/view/catalog/".$viewPath.".html", $adminView);
                if (! is_file($pluginPath . "index/view/single/".$viewPath.".html")) File::create($pluginPath . "index/view/single/".$viewPath.".html", $adminView);
                if (! is_file($pluginPath . "index/controller/$class.php")) File::create($pluginPath . "index/controller/$class.php", $indexController);
                if (! is_file($pluginPath . "index/model/$class.php")) File::create($pluginPath . "index/model/$class.php", $indexModel);
                // 菜单
                array_push($menuChildren,
                [
    				'title'    => $path . '管理',
    				'path'     => $path . '/index',
    				'ifshow'   => 1,
    				'children' => [
    					[
    						'title'      => '查看',
    						'path'       => $path . '/index',
    					],
    					[
    						'title'      => '发布',
    						'path'       => $path . '/save',
    						'logwriting' => 1,
    					],
    					[
    						'title'      => '编辑',
    						'path'       => $path . '/update',
    						'logwriting' => 1,
    					],
    					[
    						'title'      => '删除',
    						'path'       => $path . '/delete',
    						'logwriting' => 1,
    					],
    				]
                ]);
                // 路由
                array_push($pluginRoute, 
                [
                    "catalog" => $viewPath,
                    "title"   => "未定义",
                    "table"   => $table,
                ]);
            }
            // 生成插件路由
            File::create($pluginPath.'route.php', "<?php\nreturn ".var_export($pluginRoute,true).";");
            // 生成插件菜单
            $pluginMenu[] = 
            [
        		'title'    => $input['title'],
        		'path'     => $input['name'],
        		'ifshow'   => 1,
        		'children' => $menuChildren
            ];
            File::create($pluginPath.'menu.php', "<?php\nreturn ".var_export($pluginMenu,true).";");
            return json(['status' => 'success','message' => '一键生成插件成功']);
        }
    }
}
