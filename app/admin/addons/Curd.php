<?php
// 字段信息
$keywordField = [];
$viewField    = [];
$dateField    = '';
foreach ($field as $key => $val) {
    $is   = '';
    $type = '';
    switch ($val['Type']) {
        case 'text':
        case 'longtext':
            $is = 'el-editor';
            break;
        case 'time':
            $is = 'el-time-picker';
            break;
        case 'date':
            $is = 'el-date-picker';
            $type = 'date';
            break;    
        case 'datetime':
            $is = 'el-date-picker';
            $type = 'datetime';
            $dateField = $val['Field'];
            break;
    }
    if ($val['Field'] == 'id') {
        $viewField[$key]['label'] = '编号';
    } else {
        $viewField[$key]['label'] = empty($val['Comment']) ? $val['Field'] : $val['Comment'];
    }
    $viewField[$key]['prop']  = $val['Field'];
    $viewField[$key]['type']  = $type;
    $viewField[$key]['is']    = $is;
    if (strstr($val['Type'], 'varchar') !== false ||strstr($val['Type'], 'char') !== false || $val['Type'] == 'text') {
        array_push($keywordField, $val['Field']);
    }
}

// 生成前台控制器
$indexController = '<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace plugins\\'.$input['name'].'\index\controller;

use think\facade\View;
use app\index\BaseController;
use plugins\\'.$input['name'].'\index\model\\'.$class.' as '.$class.'Model;
/**
 * '.$class.'管理
 */
class '.$class.' extends BaseController
{
    /**
     * 自定义方法，访问：'.$_SERVER['SERVER_NAME'].'/'.$input['name'].'/'.lcfirst($class).'/page.html
     */
    public function page()
    {
        return json(["status" => "success", "message" => "我是应用自定义方法", "data" => []]);
        // return View::fetch("page");
    }
    
    /**
     * catalog为特殊方法，可通过分类类型绑定seo_url访问，可指定绑定html模板
     */
    public function catalog()
    {
        if ($this->request->isPost()) {
            $input = input("post.");
            $page  = empty($input["page"]) ? 1 : $input["page"];
            $count = '.$class.'Model::count();
            $data  = '.$class.'Model::page($page, 10)->select();
            return json(["status" => "success", "message" => "请求成功", "data" => $data, "count" => $count]);
        } else {
            return View::fetch("catalog/'.$viewPath.'");
        }
    }
    
   /**
     * single为特殊方法，可通过分类类型绑定seo_url访问，可指定绑定html模板
     */
    public function single()
    {
        if ($this->request->isPost()) {
            '.$class.'Model::find(input("post.id"));
            return json(["status" => "success", "message" => "请求成功", "data" => $data]);
        } else {
             return View::fetch("single/'.$viewPath.'");
        }
    }
    
    /**
     * 自定义更多方法...
     */
     
}';

// 生成后台控制器
$adminController = '<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace plugins\\'.$input['name'].'\admin\controller;

use think\facade\View;
use app\admin\BaseController;
use plugins\\'.$input['name'].'\admin\model\\'.$class.' as '.$class.'Model;
/**
 * '.$class.'管理
 */
class '.$class.' extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if ($this->request->isPost()) {
            $input = input("post.");
            $count = '.$class.'Model::withSearch(["keyword"], $input)->count();
            $data  = '.$class.'Model::withSearch(["keyword"], $input)->order($input["prop"], $input["order"])->page($input["page"], $input["pageSize"])->select();
            return json(["status" => "success", "message" => "请求成功", "data" => $data, "count" => $count]);
        } else {
            return View::fetch();
        }
    }
    
    /**
     * 保存新建的资源
     */
    public function save()
    {
        if ($this->request->isPost()) {
            '.$class.'Model::create(input("post."));
            return json(["status" => "success", "message" => "添加成功"]);
        }
    }
    
    /**
     * 保存更新的资源
     */
    public function update()
    {
        if ($this->request->isPost()) {
            '.$class.'Model::update(input("post."));
            return json(["status" => "success", "message" => "修改成功"]);
        }
    }
    
    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            '.$class.'Model::destroy(input("post.ids"));
            return json(["status" => "success", "message" => "删除成功"]);
        }
    }
}';

// 生成前台模型
$indexModel = '<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace plugins\\'.$input['name'].'\index\model;

use think\Model;

class '.$class.' extends Model
{
    protected $name = "'.$table.'";
}';

// 生成后台模型
$admminModel = '<?php
// +----------------------------------------------------------------------
// | OneKeyAdmin [ Believe that you can do better ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2023 http://onekeyadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: MUKE <513038996@qq.com>
// +----------------------------------------------------------------------
namespace plugins\\'.$input['name'].'\admin\model;

use think\Model;

class '.$class.' extends Model
{
    protected $name = "'.$table.'";
    
    // 搜索器
    public function searchKeywordAttr($query, $value)
    {
    	if (! empty($value)) {
	        $query->where("'.implode('|', $keywordField).'","like", "%" . $value . "%");
	    }
    }
    
    public function searchDateAttr($query, $value, $array)
    {
        if (! empty($value)) { 
            $query->whereBetweenTime("'.$dateField.'", $value[0], $value[1]);
        }
    }
}';

// 生成后台视图
$viewFieldStr = "[";
foreach ($viewField as $key => $val) {
    $comma = count($viewField) > $key ? "," : "";
    $type  = empty($val['type']) ? '' : ", type: '".$val['type']."'";
    $form  = empty($val['is']) ? '' : ", form: {is: '".$val['is']."'$type}";
    $viewFieldStr .= "\n\t\t\t\t\t{prop: '".$val['prop']."', label: '".$val['label']."'$form}$comma";
}
$viewFieldStr .= "\n\t\t\t\t]";
$adminView = '{include file="common/header"}
<div id="app" v-cloak>
    <el-curd :field="field"></el-curd>
</div>
<script>
    new Vue({
        el: "#app",
        data() {
            return {
                field: '.$viewFieldStr.',
            }
        },
    })
</script>
{include file="common/footer"}
';